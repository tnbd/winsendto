@echo off
rem Sökväg till impressive.exe
set impPath=C:\opt\Impressive-0.12.1-win32\Impressive.exe
rem Sökväg till aktuell mapp
set curPath=%CD%
rem Öppna alla PDF-filer i aktuell mapp
start %impPath% -ff -g 1920x1080+1920+0 --noclicks -T0 "%curPath%\*.pdf"
