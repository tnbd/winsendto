@echo off
rem Sökväg till impressive.exe
set impPath=C:\opt\Impressive-0.12.1-win32\Impressive.exe
rem Sökväg till aktuell mapp
set curPath=%CD%
rem Öppna alla PDF-filer i aktuell mapp
start %impPath% --noclicks -T0 "%curPath%\*.pdf"
